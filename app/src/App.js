import React, { useState } from 'react'
import toaster from 'toasted-notes'
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'
import './App.css'

function postData (url = '', data = {}) {
  return fetch(url, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json',
    },
    redirect: 'follow',
    referrer: 'no-referrer',
    body: JSON.stringify(data),
  }).then(response => response.json())
}

function toast (data = {}) {
  const { message = (<span>&nbsp;</span>), variant = 'secondary' } = data

  toaster.notify(
    ({ onClose }) => (
      <Alert
        className="App-notification"
        variant={variant}
        onClose={onClose}
        dismissible
      >
        {message}
      </Alert>
    ),
    {
      position: 'bottom-left',
      duration: 2000,
    }
  )
}

function App () {
  const [loggedIn, setLoggedIn] = useState(null)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const postCreds = url => postData(url, { email, password })

  async function register (e) {
    e.preventDefault()

    try {
      const result = await postCreds('/register')
      console.log('REGISTER', result)

      if (result.status === 'ok') {
        toast({ message: <span>{result.message}</span>, variant: 'primary' })
        return setLoggedIn(result.user)
      }

      else if (result.status === 'error') {
        toast({ message: <span>Registration failed. {result.message}</span>, variant: 'warning' })
      }

    } catch (err) {
      console.error('ERROR', err)
      toast({ message: <span>Registration failed. {err.message}</span>, variant: 'danger' })
    }

    setLoggedIn(null)
  }

  async function login (e) {
    e.preventDefault()

    try {
      const result = await postCreds('/login')
      console.log('LOGIN', result)

      if (result.status === 'ok') {
        toast({ message: <span>Welcome {result.user.email}</span>, variant: 'success' })
        return setLoggedIn(result.user)
      }

    } catch (err) {
      console.error('ERROR', err)
      toast({ message: <span>Authentication failed.</span>, variant: 'danger' })
    }

    setLoggedIn(null)
  }

  async function logout () {
    await fetch('/logout')
    setLoggedIn(null)
    toast({ message: 'Logged out.' })
  }

  async function getSecret () {
    try {
      const response = await fetch('/secret', { method: 'GET', credentials: 'include' })
      const result = await response.json()

      console.log('SECRET', result)

      if (result.status === 'ok') {
        toast({ message: <span>Secret: {result.secret}</span>, variant: 'success' })
      }

      else {
        toast({ message: <span>Failed to fetch secret. {response.status} {response.statusText}</span>, variant: 'warning' })
      }

    } catch (err) {
      console.error('ERROR', err)
      toast({ message: <span>Failed to fetch secret. {err.message}</span>, variant: 'danger' })
    }
  }

  return (
    <div>
      <header className="App-header App-header-font">
        {
          loggedIn
            ? (<p>User: <code>{JSON.stringify(loggedIn)}</code></p>)
            : (<p>Anonymous</p>)
        }

        <div style={{ paddingBottom: 20 }}>
          <Button onClick={getSecret}>Fetch Secret</Button>
        </div>
      </header>
      <div className="App-header">
        <Accordion defaultActiveKey="0">
          <Card>

            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="0">
                Login
              </Accordion.Toggle>
            </Card.Header>

            <Accordion.Collapse eventKey="0">

              <Card.Body>
                <Form onSubmit={login}>

                  <Form.Group controlId="loginEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" onChange={e => setEmail(e.target.value)} />
                  </Form.Group>

                  <Form.Group controlId="loginPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" onChange={e => setPassword(e.target.value)} />
                  </Form.Group>

                  <Button variant="primary" type="submit">Login</Button>

                </Form>

              </Card.Body>

            </Accordion.Collapse>
          </Card>

          <Card>

            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="1">
                Register
              </Accordion.Toggle>
            </Card.Header>

            <Accordion.Collapse eventKey="1">

              <Card.Body>
                <Form onSubmit={register}>

                  <Form.Group controlId="registerEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" onChange={e => setEmail(e.target.value)} />
                  </Form.Group>

                  <Form.Group controlId="registerPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" onChange={e => setPassword(e.target.value)} />
                  </Form.Group>

                  <Button variant="primary" type="submit">Register</Button>

                </Form>
              </Card.Body>

            </Accordion.Collapse>

          </Card>

          {
            loggedIn
              ? (
                <Card>

                  <Card.Header>
                    <Button variant="link" onClick={logout}>Logout</Button>
                  </Card.Header>

                </Card>
              ) : null
          }
        </Accordion>
      </div>
    </div>
  )
}

export default App
