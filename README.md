# NodeJS Auth Demo

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> A NodeJS server skeleton with simple forms authentication.

## Table of Contents

- [Security](#security)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Security

**A note on Bcrypt**

Bcrypt is a cryptographic one-way hash function that has a self-contained salt and that has a
configurable difficulty that comes in the form of a variable number of salt rounds to calculate a
hash. Bcrypt or hash functions similar to Bcrypt are preferable for use in storing passwords as
they are resistent to highly parallel computation and require more memory than other hash functions
such as those of the SHA family. The higher resource requirements makes it more resilient to brute
force attacks should the password hash ever be compromised -- even with large password databases.

## Install

```bash
$ npm install
```

## Usage

The NodeJS Auth Demo server accepts the following environment variables:

* `NODE_ENV`    :: Specify the deployment environment. The value `'production'` denotes a production
                   deployment and anything else is considered `'development'`.
* `PORT`        :: Specify the port that the NodeJS server listens on.
* `SALT_ROUNDS` :: Specify the difficulty of the Bcrypt hash in the number of salt rounds. More info [here](https://www.npmjs.com/package/bcrypt#a-note-on-rounds).
* `AUTH_DB`     :: Specify the folder to contain the user authentication database.

#### Development

Runs the NodeJS server and the React development server concurrently.

```bash
$ npm run dev
```

#### Production

Builds the react app and serves it from the NodeJS process.

```bash
$ npm start
```

#### Docker (Testing)

Runs the app in production mode in a temporary docker container. Serves from port 3000.

```bash
$ git clone https://gitlab.com/noroff-accelerate/javascript/nodejs-auth-demo.git
$ cd nodejs-auth-demo
$ docker run -it --rm -p 3000:3000 $(docker build -q .)
```

#### Docker (Build)

Build the Docker container.

```bash
$ git clone https://gitlab.com/noroff-accelerate/javascript/nodejs-auth-demo.git
$ cd nodejs-auth-demo
$ docker build -t registry.gitlab.com/noroff-accelerate/javascript/nodejs-auth-demo .
```

#### Docker (Production)

Run the production build from Gitlab CI/CD or local build in a daemonized docker container. Serves
from port 80.

```bash
$ docker run -d --name node-auth-demo -p 80:3000 registry.gitlab.com/noroff-accelerate/javascript/nodejs-auth-demo:latest
```

## API

The server exposes the following endpoints:

##### POST /register

Registers the user in the authentication database.

Accepts parameters:

* `email` :: Unique user identifier.
* `password` :: Plain text password for later authentication.

Accepts content types:

* `application/json`
* `application/x-www-form-urlencoded`

Responds with `403 Forbidden` if the user already exists. Responds with `201 Created` if the user
is successfully registered.

JSON Response:

* `status` :: Status enum. One of: [ `ok`, `error` ].
* `message` :: Response message.

##### POST /login

Authenticates a previously registered user.

Accepts parameters:

* `email` :: Unique user identifier.
* `password` :: Plain text password for later authentication.

Accepts content types:

* `application/json`
* `application/x-www-form-urlencoded`

Responds with `401 Unauthorized` if the authentication failed. Responds with `200 OK` if the user
is successfully authenticated.

JSON Response:

* `status` :: Status enum. One of: [ `ok`, `error` ].
* `user` :: JSON object representing the authenticated user. Only present if authentication was
            successful.

##### GET /logout

Destroys the current user session. Responds with `200 OK` even if the user wasn't initially logged
in.

JSON Response:

* `status` :: Status enum. Only [ `ok` ].

##### GET /secret

Expose a secret only to authenticated users.

Responds with `401 Unauthorized` if the user does not have an existing authenticated session.
Responds with `200 OK` if the user was previously authenticated.

JSON Response:

* `status` :: Status enum. One of: [ `ok`, `error` ].
* `secret` :: Secret message (string) that is only displayed to authenticated users.

## Maintainers

[@EternalDeiwos](https://github.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2019 Noroff Accelerate AS
