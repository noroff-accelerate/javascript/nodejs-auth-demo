'use strict'

/**
 * Dependencies
 * @ignore
 */
const cwd = process.cwd()
const path = require('path')
const express = require('express')
const session = require('express-session')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const level = require('level')
const bcrypt = require('bcrypt')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

/**
 * Module Dependencies
 * @ignore
 */

/**
 * Start
 * @ignore
 */

// Open authdb
// A simple embedded key - value store using Google's leveldb; its like SQLite for JSON data.
const authdb = level(process.env.AUTH_DB || 'authdb')

// express
const app = express()

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Cookie parser middleware
app.use(cookieParser())

// In-memory session store config
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: 'keyboard cat',
  cookie: {
    httpOnly: true,
    maxAge: 3600000, // 1 hour
  }
}))

// Setup request logger with predefined format
app.use(morgan('tiny'))

// Assign authdb to req and continue with next middleware
// Note: req is passed to each request the server receives
// IncomingRequest { } => ([err], req, res, next)
app.use((req, res, next) => {
  req.db = authdb
  next()
})

// Passport setup
app.use(passport.initialize())
app.use(passport.session())

// Instruct passport how to convert a user object into the corresponding primary key
passport.serializeUser((user, done) => done(null, user.email || user))

// Instruct passport how to fetch a user object from the database
passport.deserializeUser((email, done) => done(null, { email }))

// Local authentication strategy setup
passport.use(new LocalStrategy({
  // Field names to read off incoming requests
  usernameField: 'email',
  passwordField: 'password',

  // Remember authentication using sessions using previously configured express-session
  session: true,
  passReqToCallback: true,

  /**
   * Authentication callback
   * Must call `done` according to authentication state.
   *
   * function done (err, user, { json_response_object })
   *
   * If an error is passed to `done` then the server will respond with a 500 Internal Server Error.
   * In development mode (process.env.NODE_ENV != "production") this will also cause the error
   * to be displayed to the client.
   *
   * If a "falsey" value is passed to user then the server will respond with 401 Unauthorized.
   *
   * If a "truthy" value is passed to user then it will assume that is the user
   * and respond with a 200 OK.
   */
}, async (req, email, password, done) => {
  try {
    // Get stored bcrypt hash -- this will throw an error if not present.
    const stored = await req.db.get(email)

    // Asynchronously compare the the hashed password
    const result = await new Promise((resolve, reject) => {
      bcrypt.compare(password, stored, (err, success) => {
        if (err) {
          return reject(err)
        }

        // Resolves true or false for comparison of the hash
        resolve(success)
      })
    })

    if (result) {
      return done(null, { email })

    } else {
      /**
       * SECURITY WARNING
       *
       * This is for demonstration only and should not be used in a production setting.
       *
       * Letting the user know whether the username was wrong or the password exposes too much
       * information to an adversary. You should always respond with a uniform message along the
       * lines of "username or password invalid" without distinguishing which failed.
       */
      return done(null, false, { message: 'Incorrect password.' })
    }

  } catch (err) {
    // User doesn't exist
    if (err.notFound) {
      /**
       * SECURITY WARNING
       *
       * This is for demonstration only and should not be used in a production setting.
       *
       * Letting the user know whether the username was wrong or the password exposes too much
       * information to an adversary. You should always respond with a uniform message along the
       * lines of "username or password invalid" without distinguishing which failed.
       */
      return done(null, false, { message: 'User does not exist.' })
    }

    // Something _really_ bad happened... if you ever get here then you messed up your code.
    return done(err)
  }
}))

// Endpoints
app.post('/register', async (req, res, next) => {
  const { email, password } = req.body

  try {
    const existing = await req.db.get(email)

    if (existing) {
      // Again -- this is not a good idea from a security standpoint.
      return res.status(403).json({ status: 'error', message: 'User already exists.' })
    }
  } catch (err) {
    // User not found -- proceed with registration
    if (err.notFound) {
      // Bcrypt config
      const saltRoundsString = process.env.SALT_ROUNDS
      const saltRounds = saltRoundsString ? Number(saltRoundsString) : 10

      // Create password hash
      // Thou shalt not store your passwords in plaintext.
      const hash = await bcrypt.hash(password, saltRounds)

      // Store in db -- asynchronous operation
      await req.db.put(email, hash)

      // Notify the user of their success. Respond with 201 Created.
      // Note user is not logged in at this point.
      return res.status(201).json({ status: 'ok', message: 'User registered. Please log in.' })
    }

    // Pass error to default error handler
    return next(err)
  }
})

// Passport login endpoint.
app.post('/login',
  passport.authenticate('local'),
  (req, res) => res.status(200).json({ status: 'ok', user: req.user }))

// Clear user session.
app.get('/logout',
  (req, res) => {
    // Shortcut to clear user session.
    req.logout()
    res.status(200).json({ status: 'ok' })
  })

// Protected endpoint
app.get('/secret', (req, res) => {
  if (!req.isAuthenticated()) {
    return res.status(401).json({ status: 'error' })
  }

  res.status(200).json({ status: 'ok', secret: 'hello, world!' })
})

if (process.env.NODE_ENV === 'production') {
  // When in production mode: serve the built react app
  // When in development: the API is proxied from the create-react-app server.
  app.use(express.static(path.join(cwd, 'app', 'build')))
}

// Default error handler
// If you ever see this then you've done something really wrong.
app.use((err, req, res, next) => {
  console.error(err)

  // Send error message to client while not in production mode
  if (process.env.NODE_ENV !== 'production') {
    return res.status(500).json({ status: 'error', error: err })
  }

  res.status(500).json({ status: 'error' })
})

// Start listening
const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Server running on port ${port}.`))

/**
 * Export
 * @ignore
 */
module.exports = app
